<?php

/**
 * @file
 * Contains \Drupal\profile_tab\TabInterface.
 */

namespace Drupal\profile_tab;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\user\RoleInterface;

/**
 * 
 */
interface TabInterface extends ConfigEntityInterface {

  /**
   * Appears on all user roles.
   */
  const APPEARS_ON_ALL = 'all';

  /**
   * Appears on selected user roles.
   */
  const APPEARS_ON_ROLES = 'roles';

  /**
   * {@inheritdoc}
   */
  public function isDefault();

  /**
   * {@inheritdoc}
   */
  public function setDefault($default = true);

  /**
   * Returns the weight.
   *
   * @return int
   *   The weight of this profile tab.
   */
  public function getWeight();

  /**
   * Sets the weight to the given value.
   *
   * @param int $weight
   *   The desired weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * {@inheritdoc}
   */
  public function setFormModeId($form_mode_id);

  /**
   * {@inheritdoc}
   */
  public function getFormModeId();

  /**
   * {@inheritdoc}
   */
  public function setViewModeId($view_mode_id);

  /**
   * {@inheritdoc}
   */
  public function getViewModeId();

  /**
   * {@inheritdoc}
   */
  public function getFormMode();

  /**
   * {@inheritdoc}
   */
  public function getViewMode();

  /**
   * {@inheritdoc}
   */
  public function setFormMode(EntityFormDisplayInterface $form_mode);

  /**
   * {@inheritdoc}
   */
  public function setViewMode(EntityViewDisplayInterface $view_mode);

  /**
   * {@inheritdoc}
   */
  public function setAppearsOn($appears);

  /**
   * {@inheritdoc}
   */
  public function getAppearsOn();

  /**
   * {@inheritdoc}
   */
  public function hasRole(RoleInterface $role);

  /**
   * {@inheritdoc}
   */
  public function addRole(RoleInterface $role);

  /**
   * {@inheritdoc}
   */
  public function removeRole(RoleInterface $role);

  /**
   * {@inheritdoc}
   */
  public function getRole($rid);

  /**
   * {@inheritdoc}
   */
  public function getRoles();
}
