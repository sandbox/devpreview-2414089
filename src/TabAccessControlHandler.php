<?php

/**
 * @file
 * Contains \Drupal\profile_tab\TabAccessControlHandler.
 */

namespace Drupal\profile_tab;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the profile tab entity type.
 *
 * @see \Drupal\profile_tab\Entity\Tab
 */
class TabAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, $langcode, AccountInterface $account) {
    switch ($operation) {
      case 'delete':
        if ($entity->id() == 'main' || $entity->isDefault()) {
          return AccessResult::forbidden();
        }
      default:
        return parent::checkAccess($entity, $operation, $langcode, $account);
    }
  }

}
