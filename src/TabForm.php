<?php

/**
 * @file
 * Contains \Drupal\profile_tab\TabForm.
 */

namespace Drupal\profile_tab;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile_tab\Entity\Tab as TabEntity;

/**
 * Form controller for the profile tab entity edit forms.
 */
class TabForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\profile_tab\Entity\Tab */
    $entity = $this->entity;

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Profile tab name'),
      '#default_value' => $entity->label(),
      '#size' => 30,
      '#required' => TRUE,
      '#maxlength' => 64,
      '#description' => $this->t('The human-readable name of this profile tab.'),
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#required' => TRUE,
      '#disabled' => !$entity->isNew(),
      '#size' => 30,
      '#maxlength' => 64,
      '#machine_name' => array(
        'exists' => ['\Drupal\profile_tab\Entity\Tab', 'load'],
      ),
    );

    $form['weight'] = array(
      '#type' => 'value',
      '#value' => $entity->getWeight(),
    );

    if ($entity->isNew() || $entity->id() != 'main') {
      $form_modes = array();
      foreach (\Drupal::entityManager()->getFormModes('user') as $form_mode_id => $form_mode) {
        $form_modes[$form_mode_id] = $form_mode['label'];
      }

      $form['form_mode_id'] = array(
        '#type' => 'select',
        '#title' => $this->t('User account form mode'),
        '#required' => TRUE,
        '#default_value' => $entity->getFormModeId(),
        '#options' => $form_modes
      );

      $view_modes = array();
      foreach (\Drupal::entityManager()->getViewModes('user') as $view_mode_id => $view_mode) {
        $view_modes[$view_mode_id] = $view_mode['label'];
      }

      $form['view_mode_id'] = array(
        '#type' => 'select',
        '#title' => $this->t('User account view mode'),
        '#required' => TRUE,
        '#default_value' => $entity->getViewModeId(),
        '#options' => $view_modes
      );

      $options = array(
        TabEntity::APPEARS_ON_ALL => $this->t('All user roles'),
        TabEntity::APPEARS_ON_ROLES => $this->t('Choice user roles')
      );

      if ($entity->isDefault()) {
        $options = array(
          TabEntity::APPEARS_ON_ALL => $this->t('All user roles')
        );
        $entity->setAppearsOn(TabEntity::APPEARS_ON_ALL);
      }

      $form['appears_on'] = array(
        '#type' => 'radios',
        '#title' => $this->t('Appears on'),
        '#required' => true,
        '#default_value' => ($entity->isNew()) ? TabEntity::APPEARS_ON_ALL : $entity->getAppearsOn(),
        '#options' => $options
      );

      $roles = array_map(array('\Drupal\Component\Utility\String', 'checkPlain'), user_role_names(TRUE));
      $form['roles_id'] = array(
        '#type' => 'checkboxes',
        '#title' => $this->t('Roles'),
        '#default_value' => $entity->getRolesId(),
        '#options' => $roles,
        '#states' => array(
          'visible' => array(
            ':input[name="appears_on"]' => array('value' => TabEntity::APPEARS_ON_ROLES)
          )
        )
      );
    }

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    // Change the roles array to a list of enabled roles.
    // @todo: Alter the form state as the form values are directly extracted and
    //   set on the field, which throws an exception as the list requires
    //   numeric keys. Allow to override this per field. As this function is
    //   called twice, we have to prevent it from getting the array keys twice.
    if (is_string(key($form_state->getValue('roles_id')))) {
      $form_state->setValue('roles_id', array_keys(array_filter($form_state->getValue('roles_id'))));
    }
    return parent::buildEntity($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Prevent leading and trailing spaces in profile type names.
    $entity->set('label', trim($entity->label()));
    $status = parent::save($form, $form_state);

    $edit_link = $this->entity->link($this->t('Edit'));
    if ($status == SAVED_UPDATED) {
      drupal_set_message($this->t('Profile tab %label has been updated.', array('%label' => $entity->label())));
      $this->logger('profile_tab')->notice('Profile tab %label has been updated.', array('%label' => $entity->label(), 'link' => $edit_link));
    }
    else {
      drupal_set_message($this->t('Profile tab %label has been added.', array('%label' => $entity->label())));
      $this->logger('profile_tab')->notice('Profile tab %label has been added.', array('%label' => $entity->label(), 'link' => $edit_link));
    }
    $form_state->setRedirect($this->entity->urlInfo('collection')->getRouteName());
    return $status;
  }

}
