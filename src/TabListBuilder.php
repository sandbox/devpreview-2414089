<?php

/**
 * @file
 * Contains \Drupal\profile_tab\TabListBuilder.
 */

namespace Drupal\profile_tab;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\String;
use Drupal\profile_tab\Entity\Tab as TabEntity;

/**
 * Defines a class to build a listing of profile tab entities.
 *
 * @see \Drupal\profile_tab\Entity\Tab
 */
class TabListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'profile_tab_admin_tabs_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['form_mode'] = $this->t('Form mode');
    $header['view_mode'] = $this->t('View mode');
    $header['roles'] = $this->t('Roles');
    $header['default'] = $this->t('Default');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\profile_tab\Entity\Tab */
    $row['label'] = $this->getLabel($entity);
    $row['form_mode'] = array('#markup' => String::checkPlain(\Drupal::entityManager()->getFormModes('user')[$entity->getFormModeId()]['label']));
    $row['view_mode'] = array('#markup' => String::checkPlain(\Drupal::entityManager()->getViewModes('user')[$entity->getViewModeId()]['label']));
    $row['roles'] = array();
    if ($entity->getAppearsOn() == TabEntity::APPEARS_ON_ALL) {
      $row['roles'] = array('#markup' => String::checkPlain($this->t('All user roles')));
    }
    else {
      $roles = array();
      foreach ($entity->getRoles() as $role) {
        $roles[$role->id()] = $role->label();
      }
      asort($roles);
      $row['roles']['data'] = array(
        '#theme' => 'item_list',
        '#items' => $roles
      );
    }
    $attributes = array('value' => $entity->id());
    if ($entity->isDefault()) {
      $attributes['checked'] = 'checked';
    }
    if ($entity->getAppearsOn() != TabEntity::APPEARS_ON_ALL) {
      $attributes['disabled'] = 'disabled';
    }
    $row['default_tab'] = array(
      '#type' => 'radio',
      '#name' => 'default_tab',
      '#attributes' => $attributes
    );
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @todo: Fix me! */
    $default_tab_id = \Drupal::request()->request->get('default_tab', 'main');
    $default = false;
    foreach ($this->entities as $entity) {
      /* @var $entity \Drupal\profile_tab\Entity\Tab */
      if ($default_tab_id == $entity->id() && $default === false) {
        $entity->setDefault(true);
        $default = true;
      }
      else {
        $entity->setDefault(false);
      }
      $entity->save();
    }
    if ($default === false) {
      TabEntity::load('main')->setDefault()->save();
    }
    parent::submitForm($form, $form_state);
    drupal_set_message(t('The profile tabs settings have been updated.'));
  }

}
