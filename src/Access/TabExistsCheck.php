<?php

/**
 * @file
 * Contains Drupal\profile_tab\Access\TabExistsCheck.
 */

namespace Drupal\profile_tab\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\profile_tab\Entity\Tab as TabEntity;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Access for existing profile tabs.
 * @todo: Fixme!
 */
class TabExistsCheck implements AccessInterface {

  /**
   * Checks exists the profile tab on the given route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    /* @var $user \Drupal\user\Entity\User */
    $user = $route_match->getParameters()->get('user');

    $tab_id = $route_match->getParameter('profile_tab');
    $tab = TabEntity::load($tab_id);

    if (!$tab || ($tab->isDefault() && ($route_match->getRouteName() == 'entity.user.profile_tab' || $route_match->getRouteName() == 'entity.user.profile_tab.edit_form')) || ($tab->getAppearsOn() == TabEntity::APPEARS_ON_ROLES && !array_intersect($user->getRoles(), $tab->getRolesId()))) {
      if ($this->compareRouteMatches(\Drupal::routeMatch(), $route_match)) {
        throw new NotFoundHttpException;
      }
      else {
        return AccessResult::forbidden();
      }
    }
    else {
      return AccessResult::allowed();
    }
  }

  protected function compareRouteMatches(RouteMatchInterface $route_a, RouteMatchInterface $route_b) {
    return ($route_a->getRouteName() == $route_b->getRouteName() && count(array_diff_assoc($route_a->getRawParameters()->all(), $route_b->getRawParameters()->all())) == 0);
  }

}
