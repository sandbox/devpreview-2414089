<?php

/**
 * @file
 * Contains Drupal\profile_tab\Access\TabAccessCheck.
 */

namespace Drupal\profile_tab\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\profile_tab\Entity\Tab as TabEntity;

/**
 * Access check for profile tabs.
 */
class TabAccessCheck implements AccessInterface {

  /**
   * Checks access to the profile tab on the given route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    /* @var $user \Drupal\user\Entity\User */
    $user = $route_match->getParameters()->get('user');

    $entity_access = $user->access($this->getOperation($route), $account, true);
    if (!$entity_access->isAllowed()) {
      return $entity_access;
    }

    $tab = null;
    if ($route_match->getRouteName() == 'entity.user.canonical' || $route_match->getRouteName() == 'entity.user.edit_form') {
      $tab = TabEntity::loadDefault();
    }
    else {
      $tab_id = $route_match->getParameter('profile_tab');
      $tab = TabEntity::load($tab_id);
    }

    $result = AccessResult::allowedIfHasPermission($account, $this->getOperation($route) . ' any ' . $tab->id() . ' profile tab');

    if (!$result->isAllowed() && $user->id() == $account->id()) {
      $result = AccessResult::allowedIfHasPermission($account, $this->getOperation($route) . ' own ' . $tab->id() . ' profile tab');
    }

    if (!$result->isAllowed()) {
      foreach ($user->getRoles() as $role_id) {
        if ($result->isAllowed()) {
          break;
        }
        $result = AccessResult::allowedIfHasPermission($account, $this->getOperation($route) . ' ' . $role_id . ' ' . $tab->id() . ' profile tab');
      }
    }

    return $result;
  }

  protected function getOperation(Route $route) {
    if ($route->hasDefault('_controller')) {
      throw new AccessDeniedHttpException;
    }
    if ($route->hasDefault('_entity_form')) {
      return 'update';
    }
    if ($route->hasDefault('_entity_list')) {
      return 'list';
    }
    if ($route->hasDefault('_entity_view')) {
      return 'view';
    }
    throw new AccessDeniedHttpException;
  }

}
