<?php

/**
 * @file
 * Contains \Drupal\profile_tab\TabPermissions.
 */

namespace Drupal\profile_tab;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\profile_tab\Entity\Tab as TabEntity;

/**
 * Defines a class containing permission callbacks.
 */
class TabPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of profile tab permissions.
   *
   * @return array
   */
  public function tabPermissions() {
    $permissions = array();
    $roles = array_map(array('\Drupal\Component\Utility\String', 'checkPlain'), user_role_names(TRUE));

    foreach (TabEntity::loadMultiple() as $tab_id => $tab) {
      $permissions['view own ' . $tab->id() . ' profile tab'] = array(
        'title' => $this->t('%tab_name: View own profile tab', array('%tab_name' => $tab->label()))
      );

      $permissions['update own ' . $tab->id() . ' profile tab'] = array(
        'title' => $this->t('%tab_name: Edit own profile tab', array('%tab_name' => $tab->label()))
      );


      $permissions['view any ' . $tab->id() . ' profile tab'] = array(
        'title' => $this->t('%tab_name: View any profile tab', array('%tab_name' => $tab->label())),
        'restrict access' => true
      );

      $permissions['update any ' . $tab->id() . ' profile tab'] = array(
        'title' => $this->t('%tab_name: Edit any profile tab', array('%tab_name' => $tab->label())),
        'restrict access' => true
      );

      foreach ($roles as $role_id => $role) {
        $permissions['view ' . $role_id . ' ' . $tab->id() . ' profile tab'] = array(
          'title' => $this->t('%tab_name: View %role profile tab', array(
            '%tab_name' => $tab->label(),
            '%role' => $role
          )),
          'restrict access' => true
        );

        $permissions['update ' . $role_id . ' ' . $tab->id() . ' profile tab'] = array(
          'title' => $this->t('%tab_name: Edit %role profile tab', array(
            '%tab_name' => $tab->label(),
            '%role' => $role
          )),
          'restrict access' => true
        );
      }
    }
    return $permissions;
  }

}
