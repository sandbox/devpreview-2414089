<?php

/**
 * @file
 * Contains \Drupal\profile_tab\Form\TabDelete.
 */

namespace Drupal\profile_tab\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a deletion confirmation form for profile tab entity.
 */
class TabDelete extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the profile tab %label?', array('%label' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->urlInfo('collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $this->logger('profile_tab')->notice('Profile tab %label has been deleted.', array('%label' => $this->entity->label()));
    drupal_set_message($this->t('Profile tab %label has been deleted.', array('%label' => $this->entity->label())));
    $form_state->setRedirect($this->entity->urlInfo('collection')->getRouteName());
  }

}
