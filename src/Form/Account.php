<?php

/**
 * @file
 * Contains \Drupal\profile_tab\Form\Account.
 */

namespace Drupal\profile_tab\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the user entity edit forms.
 */
class Account extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);
    drupal_set_message($this->t('The changes have been saved.'));
    return $status;
  }

}
