<?php

/**
 * @file
 * Contains \Drupal\profile_tab\Routing\RouteSubscriber.
 */

namespace Drupal\profile_tab\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $routes = array(
      $collection->get('entity.user.canonical'),
      $collection->get('entity.user.edit_form')
    );
    foreach ($routes as $route) {
      $requirements = $route->getRequirements();
      unset($requirements['_entity_access']);
      $route->setRequirements($requirements);
      $route->addRequirements(array('_access_profile_tab' => 'TRUE'));
    }
  }

}
