<?php

/**
 * @file
 * Contains \Drupal\profile_tab\Entity\Tab.
 */

namespace Drupal\profile_tab\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\profile_tab\TabInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\user\RoleInterface;
use Drupal\user\Entity\Role as RoleEntity;

/**
 * Defines the profile tab entity class.
 *
 * @ConfigEntityType(
 *   id    = "profile_tab",
 *   label = @Translation("Profile tab"),
 *   handlers = {
 *     "access"         = "Drupal\profile_tab\TabAccessControlHandler",
 *     "list_builder"   = "Drupal\profile_tab\TabListBuilder",
 *     "form" = {
 *       "default" = "Drupal\profile_tab\TabForm",
 *       "delete"  = "Drupal\profile_tab\Form\TabDelete"
 *     }
 *   },
 *   admin_permission = "administer profile tab",
 *   config_prefix    = "profile_tab",
 *   static_cache     = TRUE,
 *   entity_keys = {
 *     "id"                   = "id",
 *     "label"                = "label",
 *     "default"              = "default",
 *     "weight"               = "weight",
 *     "form_mode_id"         = "form_mode_id",
 *     "view_mode_id"         = "view_mode_id",
 *     "appears_on"           = "appears_on",
 *     "roles_id"             = "roles_id"
 *   },
 *   links = {
 *     "collection"  = "/admin/structure/profile_tab",
 *     "edit-form"   = "/admin/structure/profile_tab/manage/{profile_tab}",
 *     "delete-form" = "/admin/structure/profile_tab/manage/{profile_tab}/delete"
 *   }
 * )
 */
class Tab extends ConfigEntityBase implements TabInterface {

  /**
   * Use as default profile tab.
   *
   * @var bool
   */
  protected $default;

  /**
   * The weight of this tab in administrative listings.
   *
   * @var int
   */
  protected $weight;

  /**
   * User account form display ID
   * 
   * @var string
   */
  protected $form_mode_id;

  /**
   * User account form ID
   * 
   * @var string
   */
  protected $view_mode_id;

  /**
   * User account form display 
   * 
   * @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface
   */
  protected $form_mode;

  /**
   * User account form 
   * 
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $view_mode;

  /**
   * Appears on
   * 
   * @var string
   */
  protected $appears_on;

  /**
   * Selected appears roles
   * 
   * @var array
   */
  protected $roles_id = array();

  /**
   * The roles of this profile tab.
   *
   * @var RoleInterface[]
   */
  protected $roles = array();

  /**
   * {@inheritdoc}
   */
  public function isDefault() {
    if ($this->default === true) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setDefault($default = true) {
    $this->default = $default;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight');
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormModeId($form_mode_id) {
    $this->form_mode_id = $form_mode_id;
    $this->form_mode = null;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormModeId() {
    return $this->form_mode_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setViewModeId($view_mode_id) {
    $this->view_mode_id = $view_mode_id;
    $this->view_mode = null;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewModeId() {
    return $this->view_mode_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormMode() {
    if ($this->getFormModeId() === NULL) {
      return NULL;
    }
    if ($this->form_mode === NULL) {
      $this->form_mode = entity_get_form_display('user', 'user', $this->getFormModeId());
    }
    return $this->form_mode;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewMode() {
    if ($this->getViewModeId() === NULL) {
      return NULL;
    }
    if ($this->view_mode === NULL) {
      $this->view_mode = entity_get_display('user', 'user', $this->getViewModeId());
    }
    return $this->view_mode;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormMode(EntityFormDisplayInterface $form_mode) {
    $this->form_mode_id = $form_mode->id();
    $this->form_mode = $form_mode;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setViewMode(EntityViewDisplayInterface $view_mode) {
    $this->view_mode_id = $view_mode;
    $this->view_mode = $view_mode;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setAppearsOn($appears) {
    $this->appears_on = $appears;
    if ($this->getAppearsOn() == Tab::APPEARS_ON_ALL) {
      $this->roles_id = array();
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAppearsOn() {
    return $this->appears_on;
  }

  /**
   * {@inheritdoc}
   */
  public function hasRoleId($rid) {
    if (array_search($rid, $this->roles_id) === false) {
      return false;
    }
    else {
      return true;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addRoleId($rid) {
    if (!$this->hasRoleId($rid)) {
      $this->roles_id[] = $rid;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeRoleId($rid) {
    if (($key = array_search($rid, $this->roles_id)) !== false) {
      unset($this->roles_id[$key]);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRolesId() {
    return $this->roles_id;
  }

  /**
   * {@inheritdoc}
   */
  public function hasRole(RoleInterface $role) {
    return $this->hasRoleId($role->id());
  }

  /**
   * {@inheritdoc}
   */
  public function addRole(RoleInterface $role) {
    $this->addRoleId($role->id());
    $this->roles[$role->id()] = $role;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeRole(RoleInterface $role) {
    $this->removeRoleId($role->id());
    if (isset($this->roles[$role->id()])) {
      unset($this->roles[$role->id()]);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRole($rid) {
    if (!$this->hasRoleId($rid)) {
      return null;
    }
    if (!isset($this->roles[$rid])) {
      $this->roles[$rid] = RoleEntity::load($rid);
    }
    return $this->roles[$rid];
  }

  /**
   * {@inheritdoc}
   */
  public function getRoles() {
    $roles = array();
    foreach ($this->getRolesId() as $rid) {
      $roles[$rid] = $this->getRole($rid);
    }
    return $roles;
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    parent::postLoad($storage, $entities);
    // Sort the queried profile tabs by their weight.
    // See \Drupal\Core\Config\Entity\ConfigEntityBase::sort().
    uasort($entities, 'static::sort');
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    if (!isset($this->weight) && ($tabs = $storage->loadMultiple())) {
      // Set a role weight to make this new role last.
      $max = array_reduce($tabs, function($max, $tab) {
        return $max > $tab->weight ? $max : $tab->weight;
      });
      $this->weight = $max + 1;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    // Clear render cache.
    entity_render_cache_clear();
    // Rebuild routes
    \Drupal::service('router.builder')->rebuild();
    \Drupal::entityManager()->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
    // Clear render cache.
    entity_render_cache_clear();
    // Rebuild routes
    \Drupal::service('router.builder')->rebuild();
    \Drupal::entityManager()->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   *
   * @return static
   *   The entity object or NULL if there is no entity with default option.
   */
  public static function loadDefault() {
    $entity_manager = \Drupal::entityManager();
    $default = current($entity_manager->getStorage($entity_manager->getEntityTypeFromClass(get_called_class()))->loadByProperties(array('default' => true)));
    if (!$default) {
      $default = self::load('main');
    }
    return $default;
  }

}
